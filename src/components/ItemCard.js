
// rafc  ( arrow function )
// rfc 
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect } from 'react';

function ItemCard(props) {
  
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src= {props.user.avatar} />
      <Card.Body>
        <Card.Title> {props.user.name} </Card.Title>
        <Card.Text>
            {props.user.email}
        </Card.Text>
        <Button variant="warning"  >Transform</Button>
      </Card.Body>
    </Card>
  );
}

export default ItemCard;